## One Card Dugeon base/insert

A 3D printed base/insert for _Barny Skinner_'s **One Card Dungeon** board game (_Pixel Collection/Matagot_ version). Inspired by Joseph Propati's great work.

![test](https://framagit.org/regisburin/ocd_base/-/raw/main/images/OCD_base_000.jpg "")

Holds the cards and dices (14mm) while playing. Fits perfectly in the box !

Print [this one part thing](https://framagit.org/regisburin/ocd_base/-/blob/main/OCD_base.stl). Carefully break/separate the two parts (the gap between 2 parts is 0,2mm). Sand/paint/pimp if needed. Enjoy...

W.I.P: Optional walls and walls holder...

Licence: CC0 - Public Domain (do WTF you want to with these files 😉 )
